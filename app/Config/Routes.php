<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('UserController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/home', 'UserController::index');
//$routes->get('/reset_password', 'UserController::reset_password_page');

$routes->get('/register_already_email', 'UserController::register_already_email');
$routes->get('/register_confirm_email', 'UserController::register_confirm_email');
$routes->get('/register_success', 'UserController::register_success');
$routes->get('/resendemail', 'UserController::ResendEmail');

//login
$routes->get('/homepage', 'UserController::homepage');
$routes->get('/profile', 'UserController::profile');
$routes->get('/accounts', 'UserController::Profile_Account');
$routes->get('/logout', 'UserController::User_Logout');

//admin
$routes->get('/showuser', 'AdminController::showuser');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

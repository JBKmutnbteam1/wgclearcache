<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>ข้อมูลลูกค้า</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/validate.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/form.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/navbar.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/dropdown.css'); ?>" type="text/css" media="screen">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/table.css'); ?>" type="text/css" media="screen">
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css'>
</head>

<body>
<?php
  $this->session = \Config\Services::session();
  if($this->session->get("Role_name") == 'student'){
    $role = 'นักเรียน';
   }else if($this->session->get("Role_name") == 'teacher'){
    $role = 'คุณครู';
   }else if($this->session->get("Role_name") == 'admin'){
     $role = 'ผู้ดูแล';
  }
    ?>
<header>
		<a href="<?php echo base_url('/home');?>"><div class="logo"><img src="<?php echo base_url('assets/img/logo.png');?>"></div></a>
					<form action="#">
					<div class="wrap">
						<div class="search">
							<input type="text" class="searchTerm" placeholder="ค้นหาได้ที่นี่">
							<button type="submit" class="searchButton">
								<i class="fa fa-search"></i>
							</button>
						</div>
						</div>
						
					</form>
					
      <div class="navbar">
          <label for="profile2" class="profile-dropdown">
            <input type="checkbox" id="profile2">
            <?php
            if($this->session->get("Picture")){?>
              <img src="<?php echo $this->session->get("Picture")?>"><?php
            }else{?>
            <img src="<?php echo base_url('assets/img/profile.jpg');?>"><?php
            }
            ?>
            <span><?php 
                    echo $role.' '.$this->session->get("Full_name"); 	
                ?></span>
            <label for="profile2"><i class="mdi mdi-menu"></i></label>
            <ul>
          <li><a href="<?php echo base_url('/profile');?>"><i class="mdi mdi-account"></i>Profile</a></li>
          <?php
                if($this->session->get("Role_name") == 'student')
                {
                ?>
          <li><a href="<?= site_url('/UserController/updatetoteacherpage')?>"><i class="mdi mdi-settings"></i>สอนบน Workgress</a></li>
          <?php
                }else if($this->session->get("Role_name") == 'admin'){
            ?>
          <li><a href="<?php echo base_url('/showuser');?>"><i class="mdi mdi-logout"></i>จัดการ USER</a></li>
          <?php
                }
                ?>
          <li><a href="#"><i class="mdi mdi-logout"></i>Course</a></li>
          <li><a href="<?= site_url('/UserController/User_Logout')?>"><i class="mdi mdi-logout"></i>Logout</a></li>
            </ul>
          </label>
        </div>
				
                    
	</header>
  <section class="hero">
		<div class="background-image" style="background-image: url(<?= base_url('assets/img/bg.png');?>);"></div>
<div class="container">
  <div class="row">
    <div class="col-12">
<div class="card">
  <div class="card-header">
    ข้อมูลลูกค้า
  </div>
  <br>
    <form action="<?= site_url('/AdminController/insert')?>" method="get">
      <button type="submit" class="btn btn-info ">เพิ่มลูกค้า</button> 
    </form>
  <div class="card-body">
    <h5 class="card-title">ลูกค้า</h5>
    <p class="card-text">

    <?php 
    $count_user_normal = 0;
    foreach($data as $row) : 
      $count_user_normal++;
    endforeach;
    $count_user_google = 0;
    foreach($data2 as $row2) : 
      $count_user_google++;
    endforeach;
    $count_user_facebook = 0;
    foreach($data3 as $row3) : 
      $count_user_facebook++;
    endforeach;
    ?>
    <br><br><br>
    มีผู้ใช้งาน workgress ทั้งหมด <?php echo $count_user_normal+$count_user_google+$count_user_facebook ?> คน<br>
    ผู้ใช้งานธรรมดา มีทั้งหมด <?php echo $count_user_normal ?> คน
    <table id="customers">
        <tr>
              <th>USER_ID</th>
              <th>First_Name</th>
              <th>Last_Name</th>
              <th>Role_Name</th>
              <th>อีเมล</th>
              <th>รูป</th>
              <th>สถานะยืนยันอีเมล</th>
              <th>ประเภทของผู้ใช้</th>
              <th>แก้ไข</th>
              <th>ลบ</th>
        </tr>
        <?php 
            foreach($data as $row) :  
        ?>
        <tr>
            <td><?php echo $row['user_id']?></td>
            <td><?php echo $row['first_name']?></td>
            <td><?php echo $row['last_name']?></td>
            <td><?php echo $row['role_name']?></td>
            <td><?php echo $row['email']?></td>
            <td><?php
            if($row['picture']){?>
              <img src="<?php echo $row['picture'] ?>" width="50px" height="50px"><?php
            }else{?>
            <img src="<?php echo base_url('assets/img/profile.jpg');?>" width="50px" height="50px"><?php
            }
            ?></td>
            <td><?php echo $row['activated']?></td>
            <td><?php echo $row['user_login_type']?></td>
            <td><a href="<?= site_url('/AdminController/update/'.$row['user_id'])?>" class="btn btn-warning">แก้ไข</a></td>
            <td><a href="<?= site_url('/AdminController/delete/'.$row['user_id'])?>" class="btn btn-danger btn-delete">ลบ</a></td>
        </tr>
        <?php
            endforeach;
        ?>
    </table>

    <br><br><br>
    ผู้ใช้งาน ของ google มีทั้งหมด <?php echo $count_user_google ?> คน
    <table id="customers">
        <tr>
              <th>USER_ID</th>
              <th>First_Name</th>
              <th>Last_Name</th>
              <th>Role_Name</th>
              <th>อีเมล</th>
              <th>รูป</th>
              <th>สถานะยืนยันอีเมล</th>
              <th>ประเภทของผู้ใช้</th>
              <th>แก้ไข</th>
              <th>ลบ</th>
        </tr>
        <?php 
            foreach($data2 as $row2) :  
        ?>
        <tr>
            <td><?php echo $row2['user_id']?></td>
            <td><?php echo $row2['first_name']?></td>
            <td><?php echo $row2['last_name']?></td>
            <td><?php echo $row2['role_name']?></td>
            <td><?php echo $row2['email']?></td>
            <td><?php
            if($row2['picture']){?>
              <img src="<?php echo $row2['picture'] ?>" width="50px" height="50px"><?php
            }else{?>
            <img src="<?php echo base_url('assets/img/profile.jpg');?>" width="50px" height="50px"><?php
            }
            ?></td>
            <td><?php echo $row2['activated']?></td>
            <td><?php echo $row2['user_login_type']?></td>
            <td><a href="<?= site_url('/AdminController/update/'.$row2['user_id'])?>" class="btn btn-warning">แก้ไข</a></td>
            <td><a href="<?= site_url('/AdminController/delete/'.$row2['user_id'])?>" class="btn btn-danger btn-delete">ลบ</a></td>
        </tr>
        <?php
            endforeach;
        ?>
    </table>

    <br><br><br>
    ผู้ใช้งาน ของ facebook มีทั้งหมด <?php echo $count_user_facebook ?> คน
    <table id="customers">
        <tr>
              <th>USER_ID</th>
              <th>First_Name</th>
              <th>Last_Name</th>
              <th>Role_Name</th>
              <th>อีเมล</th>
              <th>รูป</th>
              <th>สถานะยืนยันอีเมล</th>
              <th>ประเภทของผู้ใช้</th>
              <th>แก้ไข</th>
              <th>ลบ</th>
        </tr>
        <?php 
            foreach($data3 as $row3) :  
        ?>
        <tr>
            <td><?php echo $row3['user_id']?></td>
            <td><?php echo $row3['first_name']?></td>
            <td><?php echo $row3['last_name']?></td>
            <td><?php echo $row3['role_name']?></td>
            <td><?php echo $row3['email']?></td>
            <td><?php
            if($row3['picture']){?>
              <img src="<?php echo $row3['picture'] ?>" width="50px" height="50px"><?php
            }else{?>
            <img src="<?php echo base_url('assets/img/profile.jpg');?>" width="50px" height="50px"><?php
            }
            ?></td>
            <td><?php echo $row3['activated']?></td>
            <td><?php echo $row3['user_login_type']?></td>
            <td><a href="<?= site_url('/AdminController/update/'.$row3['user_id'])?>" class="btn btn-warning">แก้ไข</a></td>
            <td><a href="<?= site_url('/AdminController/delete/'.$row3['user_id'])?>" class="btn btn-danger btn-delete">ลบ</a></td>
        </tr>
        <?php
            endforeach;
        ?>
    </table>


  </div>
</div>
    </div>
  </div>
</div>
</section>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>ข้อมูลลูกค้า</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/validate.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/form.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/navbar.css'); ?>" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/dropdown.css'); ?>" type="text/css" media="screen">
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css'>
</head>
<body>
<?php
    $this->session = \Config\Services::session();
    if($this->session->get("Role_name") == 'student'){
        $role = 'นักเรียน';
     }else if($this->session->get("Role_name") == 'teacher'){
        $role = 'คุณครู';
     }else if($this->session->get("Role_name") == 'admin'){
$role = 'ผู้ดูแล';
}
    ?>
<header>
		<a href="<?php echo base_url('/home');?>"><div class="logo"><img src="<?php echo base_url('assets/img/logo.png');?>"></div></a>
					<form action="#">
					<div class="wrap">
						<div class="search">
							<input type="text" class="searchTerm" placeholder="ค้นหาได้ที่นี่">
							<button type="submit" class="searchButton">
								<i class="fa fa-search"></i>
							</button>
						</div>
						</div>
						
					</form>
					

                    <div class="navbar">
                    <label for="profile2" class="profile-dropdown">
                    <input type="checkbox" id="profile2">
                    <?php
                    if($this->session->get("Picture")){?>
                    <img src="<?php echo $this->session->get("Picture")?>"><?php
                    }else{?>
                        <img src="<?php echo base_url('assets/img/profile.jpg');?>"><?php
                    }
                    ?>
                    <span><?php 
                                            echo $role.' '.$this->session->get("Full_name"); 	
                                    ?></span>
                    <label for="profile2"><i class="mdi mdi-menu"></i></label>
                    <ul>
                        <li><a href="<?php echo base_url('/profile');?>"><i class="mdi mdi-account"></i>Profile</a></li>
                        <?php
                                    if($this->session->get("Role_name") == 'student' )
                                    {
                                    ?>
                        <li><a href="<?= site_url('/UserController/updatetoteacherpage')?>"><i class="mdi mdi-settings"></i>สอนบน Workgress</a></li>
                        <?php
                                    }else if($this->session->get("Role_name") == 'admin'){
                            ?>
                        <li><a href="<?php echo base_url('/showuser');?>"><i class="mdi mdi-logout"></i>จัดการ USER</a></li>
                        <?php
                                    }
                                    ?>
                        <li><a href="#"><i class="mdi mdi-logout"></i>Course</a></li>
                        <li><a href="<?= site_url('/UserController/User_Logout')?>"><i class="mdi mdi-logout"></i>Logout</a></li>
                    </ul>
                    </label>
                    </div>
				
                    
    </header>
    <section class="hero">
		<div class="background-image" style="background-image: url(<?= base_url('assets/img/bg.png');?>);"></div>
    <?php
    foreach($data as $row) :
    ?>
    <div class="container">
    <br><br>
    <h5 class="card-title">แก้ไขข้อมูลลูกค้า</h5>
    <div class="row">
        <div class="col-12">
            <form action="<?= site_url('/AdminController/update_indatabase')?>" method="post" id="form-update">

        <label for="input-id" class="col-sm-2">ID</label>
        <input type="text" name="user_id" id="input" class="form-control" value="<?= $row['user_id']?>" readonly>

        <label for="input-id" class="col-sm-2">ชื่อจริง</label>
        <input type="text" name="first_name" id="input" class="form-control" value="<?= $row['first_name']?>" >

        <label for="input-id" class="col-sm-2">role</label><br>
            <select name="role_id" id="role_id">
            <option value="<?= $row['role_id']?>">
            <?php 
            if($row['role_id'] == '1'){
                $role="Student";
            }
            else if($row['role_id'] == '2'){
                $role="Teacher";
            }
            else if($row['role_id'] == '3'){
                $role="Admin";
            }
            echo "ตอนนี้คุณเป็น ".$role;
            ?>
            </option>
            <option value="1">Student</option>
            <option value="2">Teacher</option>
            <option value="3">Admin</option>
            </select>
        <br>
        <br>
        <?php
        endforeach;
        ?>
        </form>
 <button type="submit" class="btn btn-primary" form="form-update">ยืนยัน</button> 
 <button onclick="window.location.href = '<?php echo base_url('/showuser');?>';" class="btn btn-warning">BACK</button>
    </div>
  </div>
</div>
</section>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>ข้อมูลลูกค้า</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/validate.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/form.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/navbar.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/dropdown.css'); ?>" type="text/css" media="screen">
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css'>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
</head>
<body>
<?php
	$this->session = \Config\Services::session();
	if($this->session->get("Role_name") == 'student'){
		$role = 'นักเรียน';
	 }else if($this->session->get("Role_name") == 'teacher'){
		$role = 'คุณครู';
	 }else if($this->session->get("Role_name") == 'admin'){
$role = 'ผู้ดูแล';
}
    ?>
<header>
		<a href="<?php echo base_url('/home');?>"><div class="logo"><img src="<?php echo base_url('assets/img/logo.png');?>"></div></a>
					<form action="#">
					<div class="wrap">
						<div class="search">
							<input type="text" class="searchTerm" placeholder="ค้นหาได้ที่นี่">
							<button type="submit" class="searchButton">
								<i class="fa fa-search"></i>
							</button>
						</div>
						</div>
						
					</form>
					
					<div class="navbar">
					<label for="profile2" class="profile-dropdown">
					<input type="checkbox" id="profile2">
					<?php
					if($this->session->get("Picture")){?>
					<img src="<?php echo $this->session->get("Picture")?>"><?php
					}else{?>
						<img src="<?php echo base_url('assets/img/profile.jpg');?>"><?php
					}
					?>
					<span><?php 
											echo $role.' '.$this->session->get("Full_name"); 	
									?></span>
					<label for="profile2"><i class="mdi mdi-menu"></i></label>
					<ul>
						<li><a href="<?php echo base_url('/profile');?>"><i class="mdi mdi-account"></i>Profile</a></li>
						<?php
									if($this->session->get("Role_name") == 'student' )
									{
									?>
						<li><a href="<?= site_url('/UserController/updatetoteacherpage')?>"><i class="mdi mdi-settings"></i>สอนบน Workgress</a></li>
						<?php
									}else if($this->session->get("Role_name") == 'admin'){
							?>
						<li><a href="<?php echo base_url('/showuser');?>"><i class="mdi mdi-logout"></i>จัดการ USER</a></li>
						<?php
									}
									?>
						<li><a href="#"><i class="mdi mdi-logout"></i>Course</a></li>
						<li><a href="<?= site_url('/UserController/User_Logout')?>"><i class="mdi mdi-logout"></i>Logout</a></li>
					</ul>
					</label>
					</div>
                    
	</header>
	<section class="hero">
		<div class="background-image" style="background-image: url(<?= base_url('assets/img/bg.png');?>);"></div>
    <div class="container">
	<br><br>
	<h5 class="card-title">เพิ่มข้อมูลลูกค้า</h5>
	<div class="row">
		<div class="col-12">
    <form action="<?= site_url('/AdminController/insert_indatabase')?>" method="post" id="form-insert">

        <label for="input-id" class="col-sm-2">ชื่อจริง</label>
        <input type="text" name="first_name" id="input" class="form-control" value="" placeholder="กรอกชื่อ">

        <label for="input-id" class="col-sm-2">อีเมล</label>
        <input type="text" name="email" id="email" class="form-control" value="" placeholder="กรอกอีเมล">
		<span id="email_result"></span>

        <label for="input-id" class="col-sm-2">รหัสผ่าน</label>
        <input type="text" name="password" id="input" class="form-control" value="" placeholder="กรอกรหัสผ่าน">

        <label for="input-id" class="col-sm-2">role</label>
        <br>
        <select name="role_id" id="role_id">
            <option value="">กรอก Role ID</option>
            <option value="1">Student</option>
            <option value="2">Teacher</option>
            <option value="3">Admin</option>
        </select>
        <br>
        <br>
    </form>
 <button type="submit" class="btn btn-primary" form="form-insert">ยืนยัน</button>
 <button onclick="window.location.href = '<?php echo base_url('/showuser');?>';" class="btn btn-warning">ยกเลิก</button>
    </div>
  </div>
</div>
</section>
<!-- Check ของ Register -->
<script type="text/javascript">
 $(document).ready(function(){  
      $('#email').change(function(){  
		   var Email = $('#email').val();
           //var Email = document.getElementById("Email_Register").value; 
           console.log(Email);
           if(Email != '')  
           {  
                $.ajax({  
                     url:"<?= site_url('/AdminController/Check_Email')?>",  
                     method:"POST",  
                     data:{Email:Email},  
                     success:function(data){  
                          $('#email_result').html(data);  
                     }  
                });  
           } 
      });  
 });  
 </script>  
</body>
</html>
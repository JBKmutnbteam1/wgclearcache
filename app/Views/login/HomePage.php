

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Workgress</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>

	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/validate.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/form.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/navbar.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/dropdown.css'); ?>" type="text/css" media="screen">
	<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css'>
</head>
<body>

	<?php
	$this->session = \Config\Services::session();
	if($this->session->get("Role_name") == 'student'){
		$role = 'นักเรียน';
	 }else if($this->session->get("Role_name") == 'teacher'){
		$role = 'คุณครู';
	 }else if($this->session->get("Role_name") == 'admin'){
		$role = 'ผู้ดูแล';
	 }
    ?>
		
		<header>
		<a href="<?php echo base_url('/home');?>"><div class="logo"><img src="<?php echo base_url('assets/img/logo.png');?>"></div></a>
					<form action="#">
					<div class="wrap">
						<div class="search">
							<input type="text" class="searchTerm" placeholder="ค้นหาได้ที่นี่">
							<button type="submit" class="searchButton">
								<i class="fa fa-search"></i>
							</button>
						</div>
						</div>
						
					</form>
					
					<div class="navbar">
	  <label for="profile2" class="profile-dropdown">
      <input type="checkbox" id="profile2">
	  <?php
	  if($this->session->get("Picture")){?>
      <img src="<?php echo $this->session->get("Picture");?>"><?php
	  }
	  else{?>
		<img src="<?php echo base_url('assets/img/profile.jpg');?>"><?php
	  }
	  ?>
      <span><?php 
					echo $role.' '.$this->session->get("Full_name"); 	
					?></span>
      <label for="profile2"><i class="mdi mdi-menu"></i></label>
      <ul>
		<li><a href="<?php echo base_url('/profile');?>"><i class="mdi mdi-account"></i>Profile</a></li>
		<?php
			if($this->session->get("Role_name") == 'student' ){
			?>
			<li><a href="<?= site_url('/UserController/updatetoteacherpage')?>"><i class="mdi mdi-settings"></i>สอนบน Workgress</a></li>
		<?php
			}else if($this->session->get("Role_name") == 'admin'){
			?>
		<li><a href="<?php echo base_url('/showuser');?>"><i class="mdi mdi-logout"></i>จัดการ USER</a></li>
		<?php
					}
					?>
		<li><a href="#"><i class="mdi mdi-logout"></i>Course</a></li>
		<li><a href="<?= site_url('/UserController/User_Logout')?>"><i class="mdi mdi-logout"></i>Logout</a></li>
      </ul>
    </label>
	</div>
                    
	</header>
	<section class="hero">
		<div class="background-image" style="background-image: url(<?= base_url('assets/img/bg.png');?>);"></div>
		<h1>ยินดีต้อนรับเข้าสู่
			</h1>
			<?php
				if($this->session->get("Full_name")){
					echo "<h3>Workgrass </h3>";
					echo "สวัสดี ".$this->session->get("Full_name");
				}
				else{
					echo "<h3>workgrass คุณพร้อมที่จะเรียนรู้สิ่งใหม่หรือยัง
					ถ้าพร้อมแล้ว กดปุ่มด้านล่างนี้เลย
					เพราะการเรียนรู้ไม่มีที่สิ้นสุด</h3>";
					echo "<a class='btn' id='myBtn2' >ลงทะเบียนฟรี</a>";
				}
			?>
			
	</section>

	<footer>
		<ul>
			<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
			<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
			<li><a href="#"><i class="fa fa-snapchat-square"></i></a></li>
			<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
			<li><a href="#"><i class="fa fa-github-square""></i></a></li>
		</ul>
		<p>Made by <a href="http://tutorialzine.com/" target="_blank">tutorialzine</a>. images courtesy to <a href="http://unsplash.com/" target="_blank">unsplash</a>.</p>
		<p>No attribution required. you can remove this footer.</p>
	</footer>


</body>
</html>
 
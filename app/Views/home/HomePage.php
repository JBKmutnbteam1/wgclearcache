<?php
include('config_google.php');
include('config_facebook.php');
//session_destroy();

//google_btn
 $login_button = '<a href="'.$google_client->createAuthUrl().'"><img src="assets\img\btn_google.png" width="300" height="60"/></a>';

 //facebook_btn
 $facebook_helper = $facebook->getRedirectLoginHelper();

 $facebook_permissions = ['email']; // Optional permissions
 $facebook_login_url = $facebook_helper->getLoginUrl('https://wg-test.herokuapp.com/UserFacebookController/Facebook_Login', $facebook_permissions);
 $facebook_login_url = '<a href="'.$facebook_login_url.'"><img src="assets\img\btn_facebook.png" width="300" height="60"/></a>';

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Workgress</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/navbar.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/validate.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/form.css'); ?>" type="text/css" media="screen"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
</head>
<body>

	<?php
	$this->session = \Config\Services::session();
	?>
	<header>

<a href="<?php echo base_url('/home');?>"><div class="logo"><img src="<?php echo base_url('assets/img/logo.png');?>"></div></a>
			<form action="#">
			<div class="wrap">
				<div class="search">
					<input type="text" class="searchTerm" placeholder="ค้นหาได้ที่นี่">
					<button type="submit" class="searchButton">
						<i class="fa fa-search"></i>
					</button>
				</div>
				</div>
			</form>

		<nav>
			<li><a class="Btn_Login" id="myBtn" >เข้าสู่ระบบ</a></li>
			
			<!-- Form Login -->
			<div id="myModal" class="modal">
			<form action="<?= site_url('/UserController/User_Login')?>" method="post" id="form-login">
				<!-- Modal content -->
				<div class="modal-content">
					<span class="close">&times;</span>
					<?php
					echo '<div align="center">'.$login_button.'</button></div>';
   					?>
					<?php
					echo '<div align="center">'.$facebook_login_url .'</button></div>';
   					?>
					<label for="Email_Login">
							<input type="email" id="Email_Login" name="Email_Login" maxlength="100" minlength="8" placeholder="อีเมล" required>
							<span id="email_check_login"></span>
					</label>
					<label for="Password_Login">
						<span></span>
						<input type="password" id="Password_Login" name="Password_Login" maxlength="20" minlength="8" placeholder="รหัสผ่าน" required>
						
					</label>
					
			</form>
					<span id="uesr_check_login"></span>
					<button type="submit" form="form-login">LOGIN</button>
					<!--<a href="<?php echo base_url('reset_password');?>">ลืมรหัสผ่าน ?</a>-->
				</div>
				</div>


			<li><a class="Btn_Register" id="myBtn2" >ลงทะเบียน</a></li>
			<!-- Form Register -->
			<div id="myModal2" class="modal">
				<!-- Modal content -->
				<form action="<?= site_url('/UserController/User_Register')?>" method="post" id="form-register">
				<div class="modal-content">
					<span class="close2">&times;</span><br>
					<label for="Full_Name_Register">
						<span></span>
						<input type="text" id="Full_Name_Register" name="Full_Name_Register" minlength="3" placeholder="ชื่อ - นามสกุล" required>
						<ul class="input-requirements">
							<li>ต้องมีตัวอักษรอย่างน้อย 3 ตัวอักษร</li>
							<li>ต้องมีตัวอักษรและตัวเลขเท่านั้น (ห้ามมีตัวอักษรพิเศษ)</li>
						</ul>
					</label>
					
					<label for="Email_Register">
							<input type="email" id="Email_Register" name="Email_Register" maxlength="100" minlength="8" placeholder="อีเมล" required>
							<span id="email_result"></span>
							<ul class="input-requirements">
								<li>ต้องมีอักขระพิเศษ (เช่น @)</li>
							</ul>
					</label>
									
					<label for="Password_Register">
						<span></span>

						<input type="password" id="Password_Register" name="Password_Register" maxlength="20" minlength="8" placeholder="รหัสผ่าน" required>

						<ul class="input-requirements">
					
							<li>ต้องมีตัวอักษรอย่างน้อย 8 ตัวอักษร (และไม่เกิน 20 ตัวอักษร)</li>
							<li>ต้องมีหมายเลขอย่างน้อย 1 หมายเลข</li>
							<li>ต้องมีตัวอักษรตัวพิมพ์เล็กอย่างน้อย 1 ตัว</li>
							<li>ต้องมีตัวอักษรตัวพิมพ์ใหญ่อย่างน้อย 1 ตัว</li>
							<li>ต้องมีอักขระพิเศษ (เช่น @!)</li>
						</ul>
					</label>

					<label for="PasswordRepeat_Register">
						<span></span>
						<input type="password" id="PasswordRepeat_Register" name="PasswordRepeat_Register" maxlength="20" minlength="8" placeholder="ยืนยันรหัสผ่าน" required>
						<ul class="input-requirements">
							<li>รหัสผ่านนี้ต้องตรงกับรหัสแรก</li>
						</ul>
					</label>

				
				</form>
					<button  type="submit" form="form-register">ลงทะเบียน</button>
					</div>
				</div>
		
		</nav>
</header>

	<section class="hero">
		<div class="background-image" style="background-image: url(<?= base_url('assets/img/bg.png');?>);"></div>
		<h1>ยินดีต้อนรับเข้าสู่ Workgrass
			</h1>
			<h3>คุณพร้อมที่จะเรียนรู้สิ่งใหม่หรือยัง
					ถ้าพร้อมแล้ว กดปุ่มด้านล่างนี้เลย
					เพราะการเรียนรู้ไม่มีที่สิ้นสุด</h3>
			
	</section>
	<footer>
		<ul>
			<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
			<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
			<li><a href="#"><i class="fa fa-snapchat-square"></i></a></li>
			<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
			<li><a href="#"><i class="fa fa-github-square""></i></a></li>
		</ul>
		<p>Made by <a href="http://tutorialzine.com/" target="_blank">tutorialzine</a>. images courtesy to <a href="http://unsplash.com/" target="_blank">unsplash</a>.</p>
		<p>No attribution required. you can remove this footer.</p>
	</footer>
<script>
// Get the modal
var modal = document.getElementById("myModal");
var modal2 = document.getElementById("myModal2");
// Get the button that opens the modal
var btn = document.getElementById("myBtn");
var btn2 = document.getElementById("myBtn2");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
var span2 = document.getElementsByClassName("close2")[0];


// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}
btn2.onclick = function() {
  modal2.style.display = "block";
}


// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}
span2.onclick = function() {
  modal2.style.display = "none";
}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
  else if (event.target == modal2) {
    modal2.style.display = "none";
  }
}
</script>

<!-- Check ของ Register -->
<script type="text/javascript">
 $(document).ready(function(){  
      $('#Email_Register').change(function(){  
		   var Email = $('#Email_Register').val();
           //var Email = document.getElementById("Email_Register").value; 
           console.log(Email);
           if(Email != '')  
           {  
                $.ajax({  
                     url:"<?= site_url('/UserController/Check_Email')?>",  
                     method:"POST",  
                     data:{Email:Email}, 
                     success:function(data){  
                          $('#email_result').html(data);  
                     }  
                });  
           } 
      });  
 });  
 </script>  

<!-- Check ของ Login -->
<script type="text/javascript">
 $(document).ready(function(){  
      $('#Email_Login').change(function(){  
		   var Email_Login = $('#Email_Login').val(); 
           //var Email_Login = document.getElementById("Email_Login").value; 
		   console.log(Email_Login);
           if(Email_Login != '')  
           {  
                $.ajax({  
                     url:"<?= site_url('/UserController/Check_Email_Login')?>",  
                     method:"POST",  
                     data:{Email_Login:Email_Login},  
                     success:function(data){  
                          $('#email_check_login').html(data);  
                     }  
                });  
           } 
      });  
 });  
 </script> 

 <?php include 'Register_Validate_Script.php';?>
</body>
</html>

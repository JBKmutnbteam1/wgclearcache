<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Workgress Reset Password</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/validate.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/form.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/navbar.css'); ?>" type="text/css" media="screen"/>
</head>
<body>

	<?php
	$this->session = \Config\Services::session();
    ?>

<header>
		<a href="<?php echo base_url('/home');?>"><div class="logo"><img src="<?php echo base_url('assets/img/logo.png');?>"></div></a>
					<form action="/action_page.php">
					<div class="wrap">
						<div class="search">
							<input type="text" class="searchTerm" placeholder="ค้นหาได้ที่นี่">
							<button type="submit" class="searchButton">
								<i class="fa fa-search"></i>
							</button>
						</div>
						</div>
						
					</form>
          
	</header>
    
	<section class="hero">
    <div class="background-image" style="background-image: url(<?= base_url('assets/img/bg.png');?>);"></div>
    <br><br>
    <div class="row">
        <div class="col-12">
    <form action="<?= site_url('/UserController/Reset_Password')?>" method="get" id="form-update">
    <br>
    <h1>กรอกอีเมลเพื่อ Reset Password</h1>
    <span id="email_check_login"></span>
    <input type="email" id="Email" name="Email" >
    </form>
    <br>
    <button type="submit" class="btn btn-primary" form="form-update">ตกลง</button> 
    <button onclick="window.location.href = '<?php echo base_url('/home');?>';" class="btn btn-warning">กลับ</button>
        </div>
    </div>
    </section>


</body>
</html>
 
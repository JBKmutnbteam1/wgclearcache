<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Workgress</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/styles.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/validate.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/form.css'); ?>" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/navbar.css'); ?>" type="text/css" media="screen"/>
</head>
<body>
<header>

<a href="<?php echo base_url('/home');?>"><div class="logo"><img src="<?php echo base_url('assets/img/logo.png');?>"></div></a>
			<form action="#">
			<div class="wrap">
				<div class="search">
					<input type="text" class="searchTerm" placeholder="ค้นหาได้ที่นี่">
					<button type="submit" class="searchButton">
						<i class="fa fa-search"></i>
					</button>
				</div>
				</div>
			</form>

		<nav>
			<a href="<?php echo base_url('/home');?>">กลับไปหน้าแรก</div></a>
		</nav>
</header>
	<section class="hero">
		<div class="background-image" style="background-image: url(<?= base_url('assets/img/bg.png');?>);"></div>
		<h1>ยินดีต้อนรับเข้าสู่ Workgress
			</h1>
		<h3>มีผู้ใช้งาน Email นี้แล้ว กรุณาสมัครโดยใช้ Email อื่นๆของท่าน</h3>
			
	</section>

	<footer>
		<ul>
			<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
			<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
			<li><a href="#"><i class="fa fa-snapchat-square"></i></a></li>
			<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
			<li><a href="#"><i class="fa fa-github-square""></i></a></li>
		</ul>
		<p>Made by <a href="http://tutorialzine.com/" target="_blank">tutorialzine</a>. images courtesy to <a href="http://unsplash.com/" target="_blank">unsplash</a>.</p>
		<p>No attribution required. you can remove this footer.</p>
	</footer>

</body>

</html>

<?php
    namespace App\Controllers;
    use App\Controllers\BaseController;
    use App\Models\Admin_model;
    
    class AdminController extends BaseController{
        protected $session;
        function __construct()
        {
            $this->session = \Config\Services::session();
            $this->session->start();
        }
        public function showuser()
        {   if($this->session->get("Role_name") == 'admin'){
            $model = new Admin_model();
            $data['data'] = $model->show_user_normal(null);
            $data['data2'] = $model->show_user_google(null);
            $data['data3'] = $model->show_user_facebook(null);
            echo view('admin/Show_User',$data);
            }else{
                return redirect()->to( base_url('/home') );
            }
        }
        public function insert()
        {
            if($this->session->get("Role_name") == 'admin'){
            echo view('admin/insert');
            }else{
                return redirect()->to( base_url('/home') );
            }
            
        }
        public function insert_indatabase()
        {
            if($this->session->get("Role_name") == 'admin'){
            $first_name = $this->request->getVar('first_name');
            $email = $this->request->getVar('email');
            $password = md5($this->request->getVar('password'));
            $role_id  = $this->request->getVar('role_id');
            $model = new Admin_model();
            $model->insert_ja($first_name,$email,$password,$role_id);
                return redirect()->to( base_url('/showuser') );
            }else{
                return redirect()->to( base_url('/home') );
            }
        }
        public function update($id = null)
        {
            if($this->session->get("Role_name") == 'admin'){
            $model = new Admin_model();
            $data['data'] = $model->select($id);
            //print_r($data['data']);
            echo view('admin/update',$data);
            }else{
                return redirect()->to( base_url('/home') );
            }
        }
        public function update_indatabase()
        {
            if($this->session->get("Role_name") == 'admin'){
            $user_id = $this->request->getVar('user_id');
            $first_name = $this->request->getVar('first_name');
            $role_id  = $this->request->getVar('role_id');
                $model = new Admin_model();
                $model->update_ja($user_id,$first_name,$role_id);
                return redirect()->to( base_url('/showuser') );
            }else{
                return redirect()->to( base_url('/home') );
            }
        }
        public function delete($id = null)
        {
            if($this->session->get("Role_name") == 'admin'){
            $model = new Admin_model();
            $data['data'] = $model->select($id);
            //print_r($data['data']);
            echo view('admin/delete',$data);
            }else{
                return redirect()->to( base_url('/home') );
            }
            
        }
        public function delete_indatabase()
        {
            if($this->session->get("Role_name") == 'admin'){
            $user_id = $this->request->getVar('user_id');
                $model = new Admin_model();
                $model->delete_ja($user_id);
                return redirect()->to( base_url('/showuser') );
            }else{
                return redirect()->to( base_url('/home') );
            }
                
        }
        public function Check_Email(){
            $Email  = $this->request->getVar('Email');//request Email_Register ดึงตัวแปรมาจาก หน้าPageเพื่อใช่งาน
            $model = new Admin_model();//เรียก model User_model เพื่อใช่งานต่อ database
            if($model->Check_User_Exist($Email)){ //เป็นการเช็คว่าถ้ามีอีเมลนี้อยู่ในระบบ จะแสดง คำว่า อีเมลนี้ถูกใช้งานไปแล้ว 
                echo '<p style="color:#FF0000";> อีเมลนี้ถูกใช้งานไปแล้ว</p>';  
            }else{ //ถ้าไม่มีอีเมลนี้อยู่ในระบบ จะแสดง คำว่า อีเมลนี้สามารถใช้งานได้ 
                echo '<p style="color:#2ecc71";> อีเมลนี้สามารถใช้งานได้</p> ';  
            }
        }
    }
?>
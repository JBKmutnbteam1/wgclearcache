<?php namespace App\Controllers;

namespace App\Controllers;

use App\Models\User_model;
class UserController extends BaseController
{ 
    protected $session;
    protected $Data;
    public $get_otp;
    public function __construct()
    {
        $this->session = \Config\Services::session();
        $this->session->start();
    }
    /**** ส่วนของ View ****/
    public function index(){
        if($this->session->get("Role_name") == 'student'){
            echo view('login/HomePage');
        }else if($this->session->get("Role_name") == 'teacher'){
            echo view('login/HomePage');
        }else if($this->session->get("Role_name") == 'admin'){
            echo view('login/HomePage');
        }else{
            echo view('home/HomePage');
        }
    }
    //HomePage_Login
    public function homepage(){
        if($this->session->get("Role_name") == 'student'){
            //echo view('navbar');
            echo view('login/HomePage');
        }else if($this->session->get("Role_name") == 'teacher'){
            echo view('login/HomePage');
        }else if($this->session->get("Role_name") == 'admin'){
            echo view('login/HomePage');
        }else{
            echo view('home/HomePage');
        }
    }

    public function register_already_email(){
       
        echo view('home/Register_Already_Email');
    }
    public function register_confirm_email(){
       
        echo view('home/Register_Confirm_Email');
    }
    public function register_success(){
        echo view('home/Register_success');
    }

    public function profile(){
        if($this->session->get("Role_name") == 'student'){
            echo view('login/User_Profile');
        }else if($this->session->get("Role_name") == 'teacher'){
            echo view('login/User_Profile');
        }else if($this->session->get("Role_name") == 'admin'){
            echo view('login/User_Profile');
        }else{
            echo view('home/HomePage');
        }
    }
    public function Profile_Account(){
        if($this->session->get("Role_name") == 'student'){
            echo view('login/User_Account');
        }else if($this->session->get("Role_name") == 'teacher'){
            echo view('login/User_Account');
        }else if($this->session->get("Role_name") == 'admin'){
            echo view('login/User_Account');
        }else{
            echo view('home/HomePage');
        }
    }
    public function updatetoteacherpage(){
        if($this->session->get("Full_name")){
            echo view('login/UpdatetoTeacher');
        }
        else{
            echo view('home/HomePage');
        }
    }
    
    
   /**********************************/
    /* User_Register */
    /*
    - request มาจากหน้า HomePage กรณีที่จะสมัครสมาชิกใหม่
    - รับข้อมูลมา 3 ประเภทคือ First_Name , Email ,Password
    - hash คือ การสุ่มตัวเลขขึ้นมา เพื่อเอาไปเช็คกับตัว Email 
    - เรียก model User_model เพื่อใช่งานต่อ database
    */
    public function User_Register(){

        $Full_Name = $this->request->getVar('Full_Name_Register');//request First_Name_Register ดึงตัวแปรมาจาก หน้าPageเพื่อใช่งาน
        $Email  = $this->request->getVar('Email_Register');//request Email_Register ดึงตัวแปรมาจาก หน้าPageเพื่อใช่งาน
        $Password  = $this->request->getVar('Password_Register'); //request Password_Register ดึงตัวแปรมาจาก หน้าPageเพื่อใช่งาน
        $hash = md5(rand(0,1000)); // สุ่มตัวเลขสำหรับ เช็คEmail
        $model = new User_model(); // เรียก Model สำหรับติดต่อ Database
        $Password_md5 = md5($Password);
        if($model->Check_User_Exist($Email)){ //Check_User_Exist() คือการเช็คEmail ว่ามีอยู่ในDatabase หรือเปล่า
            return redirect()->to( base_url('register_already_email') ); 
        }else{
            $model->Insert_Register($Full_Name,$Email,$Password_md5,$hash); //Insert_Register() คือการ Insert ข้อมูลลงใน Databse
            $this->Send_Confirmation($Full_Name,$Email,$Password,$hash); //Send_Confirmation() คือการ ส่งข้อมูลการยืนยันว่าเป็นอีเมลของ User จริงหรือไม่ ไปยัง Email ที่กรอกมา
            return redirect()->to( base_url('register_confirm_email') );
        } 
    }
    /* Check_Email */
    /*
    - request มาจากหน้า HomePage กรณีที่จะสมัครสมาชิกใหม่
    - รับข้อมูล Email ของผู้สมัครมา
    - เรียก model User_model เพื่อใช่งานต่อ database
    - เอาไว้เช็คข้อมูล Email ในระบบ
    */
    public function Check_Email(){
        $Email  = $this->request->getVar('Email');//request Email_Register ดึงตัวแปรมาจาก หน้าPageเพื่อใช่งาน
        $model = new User_model();//เรียก model User_model เพื่อใช่งานต่อ database
        if($model->Check_User_Exist($Email)){ //เป็นการเช็คว่าถ้ามีอีเมลนี้อยู่ในระบบ จะแสดง คำว่า อีเมลนี้ถูกใช้งานไปแล้ว 
            echo '<p style="color:#FF0000";> อีเมลนี้ถูกใช้งานไปแล้ว</p>';  
        }else{ //ถ้าไม่มีอีเมลนี้อยู่ในระบบ จะแสดง คำว่า อีเมลนี้สามารถใช้งานได้ 
            echo '<p style="color:#2ecc71";> อีเมลนี้สามารถใช้งานได้</p> ';  
        }
    }

    /* Send_Confirmation */
    /*
    - รับข้อมูลจาก function User_Register เพื่อมาส่ง email ไปยัง email ของ User
    - ทำการเรียกใช่งานของ Services::email() เป็น library ของ CI4 
    */
    public function Send_Confirmation($Full_Name,$Email,$Password,$hash) {
        $email = \Config\Services::email();	//load email library
        $email->setFrom('pipat0909737525@gmail.com', 'Workgress'); // email ของผู้ส่ง
        $subject = "Welcome to Workgress!";	//subject
        $message = /*-----------email body starts-----------*/
          'Thanks for signing up, '.$Full_Name.'!<br>
        
          Your account has been created.<br> 
          Here are your login details.<br>
          -------------------------------------------------<br>
          Email   : ' . $Email . '<br>
          Password: ' . $Password . '<br>
          -------------------------------------------------<br>
                          
          Please click this link to activate your account:<br>
              
          ' . base_url() . '/UserController/verify?' . 
          'email=' . $Email . '&hash=' . $hash ;
        

          /*-----------email body ends-----------*/		      
          $email->setTo($Email);
          $email->setSubject($subject);
          $email->setMessage($message);
          $email->send();
      }

    /* verify */
    /*
    - รับข้อมูลจาก email 
    - ทำการเรียกใช่งานของ Services::email() เป็น library ของ CI4 
    */
    public function verify() {
        $model = new User_model();
        $GetEmail = $_GET['email'];
        $GetHash = $_GET['hash'];
        $hash_verify = $model->Select_Hash($GetEmail);
        $hash_substr = substr($hash_verify,20);

        if((int)$GetHash == (int)$hash_substr){
            //check whether the input hash value matches the hash value retrieved from the database
            $model->Verify_Register($GetEmail); //update the status of the user as verified
               /*---Now you can redirect the user to whatever page you want---*/
               //echo view('Register_success');
               echo "<script type='text/javascript'>alert('ยืนยันอีเมลเรียบร้อย');window.location.href = '" . base_url() . "/home';</script>";
        }
        
    }
    public function Check_Email_Login(){
        $model = new User_model();
        $Email_Login  = $this->request->getVar('Email_Login');
        
        if($model->Check_Email_Login($Email_Login)){ //เป็นการเช็คว่าถ้ามีอีเมลนี้อยู่ในระบบ จะแสดง คำว่า อีเมลนี้ไม่มีอยู่ในระบบ
            echo '<p style="color:#FF0000";> อีเมลนี้ไม่มีอยู่ในระบบ</p>';  
        }
    }
    public function User_Login(){
        //$session = \Config\Services::session();
        
        $model = new User_model();
        $Email_Login  = $this->request->getVar('Email_Login');
        $Password_Login = md5($this->request->getVar('Password_Login'));
        $User_Data = $model->Check_User_Pass_Login($Email_Login,$Password_Login);
        while($User = $User_Data->fetchRow()){
            $Full_Name = $User['first_name'];
            $Email = $User['email'];
            $Password = $User['password'];
            if($User['picture'] != " "){
                $Picture = $User['picture'];
            }
            $Activated = $User['activated'];
            $Role_Name = $User['role_name'];
        }
        if($Email_Login == $Email && $Password_Login == $Password && $Activated == 1  ){
            $this->Data = [
                'Full_name' => $Full_Name,
                'Role_name' => $Role_Name,
                'Email' => $Email,
                'Picture' => $Picture,
                'Type' => 'normal',
            ];
            $this->session->set($this->Data);
            return redirect()->to( base_url('homepage') );
        }else if($Email_Login == $Email && $Password_Login == $Password && $Activated == 1 ){
            $this->Data = [
                'Full_name' => $Full_Name,
                'Role_name' => $Role_Name,
                'Email' => $Email, 
                'Picture' => $Picture,
                'Type' => 'normal'
            ];
            $this->session->set($this->Data);
            return redirect()->to( base_url('homepage') );
        }else if($Email_Login == $Email && $Password_Login == $Password && $Activated == 1 && $Role_Name == 'admin' ){
            $this->Data = [
                'Full_name' => $Full_Name,
                'Role_name' => $Role_Name,
                'Email' => $Email,
                'Picture' => $Picture,
            ];
            $this->session->set($this->Data);
            return redirect()->to( base_url('homepage') );
        }else if($Email_Login == $Email && $Password_Login == $Password && $Activated == 0){

            echo "<script type='text/javascript'>alert('คุณต้องยืนยันอีเมลก่อน');window.location.href = '" . base_url() . "/home';</script>";
            //echo '<p style="color:#FF0000";>คุณต้องยืนยันอีเมลก่อน</p>';
        }else {
            
            echo "<script type='text/javascript'>alert('อีเมล หรือ รหัสผ่านผิดกรุณากรอกใหม่');window.location.href = '" . base_url() . "/home';</script>";
            //echo '<p style="color:#FF0000";>อีเมล หรือ รหัสผ่านผิดกรุณากรอกใหม่</p>';
        }
    }
    public function Update_To_Teacher(){
        $model = new User_model();
        $Email  = $this->session->get("Email");
        $Type  = $this->session->get("Type");
        if($model->Update_To_Teacher($Email,$Type)){
            $User_Data = $model->Select_Role($Email,$Type);
            while($User = $User_Data->fetchRow()){
                $Role_Name = $User['role_name'];
            }
            $this->Data = [
                'Role_name' => $Role_Name, 
            ];
            $this->session->set($this->Data);
            return redirect()->to( base_url('homepage') );
        }else{
            echo "<script type='text/javascript'>alert('มีบางอย่างผิดพลาด');window.location.href = '" . base_url() . "/home';</script>";
        }
    }
    public function Update_Profile(){
        $model = new User_model();  
        $Full_Name  = $this->request->getVar('Full_Name');
        $Email  = $this->session->get("Email");
        if($model->Update_Profile($Email,$Full_Name)){
            $User_Data = $model->Select_AllData($Email);
            while($User = $User_Data->fetchRow()){
                $Full_Name = $User['first_name'];
                $Email = $User['email'];
            }
            $this->Data = [
                'Full_name' => $Full_Name,
                'Email' => $Email,
            ];
            $this->session->set($this->Data);
            return redirect()->to( base_url('profile') );
        }else{
            echo "<script type='text/javascript'>alert('มีบางอย่างผิดพลาด');window.location.href = '" . base_url() . "/home';</script>";
        }
    }
    public function Change_Password(){
        $model = new User_model();
        $Email  = $this->session->get("Email");
        $Password_Old = md5($this->request->getVar('Password_Old'));
        $Password_New = md5($this->request->getVar('Password_New'));
        $Password_ac = md5($this->request->getVar('Password_New'));
        $User_Data = $model->Select_AllData($Email);
            while($User = $User_Data->fetchRow()){
                $Password = $User['password'];
            }
        if($Password == $Password_Old ){
            if($Password_New == $Password_Old){
                     echo "<script type='text/javascript'>alert('รหัสผ่านไม่สามารถเหมือนอันเก่าได้ หรือ มีบางอย่างผิดพลาด');window.location.href = '" . base_url() . "/home';</script>";
            }else if($Password_New == $Password_ac && $Password_New != $Password_Old){
                $model->Updata_Password($Email,$Password_New);
                return redirect()->to( base_url('logout') );  
            }
        }else{
            echo "<script type='text/javascript'>alert('รหัสผ่านเก่าไม่ตรงกับในระบบ');window.location.href = '" . base_url() . "/accounts';</script>";
            }
    }

    public function User_Logout(){
        $this->session->remove($this->Data);
        $this->session->destroy();
        return redirect()->to( base_url('home') );  
    }

    public function User_Delete(){
        $model = new User_model();  
        $Email  = $this->session->get("Email");
        $Type  = $this->session->get("Type");
        //echo $Email.' '.$Type;
        if($model->Delete_User($Email,$Type)){
            $this->session->remove($this->Data);
            $this->session->destroy();
            echo "<script type='text/javascript'>alert('ลบ Account เรียบร้อย');window.location.href = '" . base_url() . "/home';</script>";
        }else{
            echo "<script type='text/javascript'>alert('มีบางอย่างผิดพลาด');window.location.href = '" . base_url() . "/home';</script>";
        }
    }
    public function Upload_Picture(){
        $model = new User_model();  
        $Email = $this->session->get("Email");
        $Type = $this->session->get("Type");

        $Photo = $this->request->getFile('photo');
        $Photo_Random_Name = $Photo->getRandomName();
       
        //echo $Photo->getClientName();
        if($Photo->getSize() > 0){  
            $upload_to = 'public/upload/';

            $image = \Config\Services::image()
            ->withFile($Photo)
            ->fit(626, 626, 'center')
            ->save('./public/upload/'.$Photo_Random_Name);

            $Photo_Type = $upload_to.$Photo_Random_Name;
            //$Photo->move('./public/upload',$Photo_Random_Name);
            $model->Update_Picture_User($Email,$Type,$Photo_Type);
            $User_Data = $model->Select_AllData($Email);
            while($User = $User_Data->fetchRow()){
                $Picture = $User['picture'];
            }
            //echo '<img src="'.$Picture.'"/>';
            $this->Data = [
                'Picture' => $Picture,
            ];
            $this->session->set($this->Data);
            $msg = 'อัพเดทโปรไฟล์เรียบร้อย';
            return redirect()->to( base_url('profile') )->with('ok', true)->with('msg', $msg);
        }
    }
    
    
}